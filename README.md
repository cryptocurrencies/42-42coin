This Dockerfile generates the latest 42-coin client. It is anticipated that you would run the following to install it on your server:

Replacing /localdata with a location on your server where you want to keep the persistant data:
```sh
docker run -d -P --name 42coin -v /localdata:/data \
registry.gitlab.com/cryptocurrencies/42-42coin:latest
```
