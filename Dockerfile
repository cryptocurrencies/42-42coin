# ---- Base Node ----
FROM ubuntu:16.04 AS base
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y build-essential libtool autotools-dev autoconf libssl-dev git-core libboost-all-dev libdb4.8-dev libdb4.8++-dev libminiupnpc-dev pkg-config sudo nano curl cmake && \
    apt-get upgrade -y && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# ---- Build Source ----
FROM base as build
# Checkout latest Sourcecode
RUN git clone https://github.com/42-coin/42.git /opt/42coin && \
	cd /opt/42coin && \
	git checkout tags/v0.0.8 && \
    cd /opt/42coin/src && \
    make -j2 -f makefile.unix

# ---- Release ----
FROM ubuntu:16.04 as release
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y libboost-all-dev libdb4.8 libdb4.8++ libminiupnpc-dev && \
    apt-get clean && rm -rf /var/lib/apt/lists/ /tmp/ /var/tmp/*
RUN groupadd -r 42coin && useradd -r -m -g 42coin 42coin
RUN mkdir /data
RUN chown 42coin:42coin /data && \
    ln -s /data /home/42coin/.42
COPY --from=build /opt/42coin/src/42d /usr/local/bin/42d
USER 42coin
VOLUME /data
EXPOSE 4242 2121

CMD ["/usr/local/bin/42d", "-conf=/data/42.conf", "-datadir=/data", "-server", "-txindex", "-printtoconsole"]